{
  users.users.julio = {
    isNormalUser = true;
    extraGroups = [
      "networkmanager"
    ];
    initialHashedPassword = "$6$qROdxayoF6BexsPV$asDqTdzuqIoKXE6GPGb9tBqB7F8gV8TCwPP3KdXitnj9GfIE0gx24zi7ZR064eikj.TT/47R/Vg5s6I55zQ8V/";
  };
}
