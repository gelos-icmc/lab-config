{
  imports = [
    ./cicd
    ./gui
    ./julio
    ./misterio
    ./setembru
    ./tomieiro
    ./trents
  ];
  users = {
    mutableUsers = false;
  };
}
